
const checkUserLocal = localStorage.getItem('in');
const checkUserSession = sessionStorage.getItem('in');
if(!checkUserLocal && !checkUserSession)
    window.location.href = 'logIn.html';

function Product(name, category, price, valute, newOrUsed, img) {
    this.name = name;
    this.category = category;
    this.price = price;
    this.valute = valute;
    this.newOrUsed = newOrUsed;
    this.img = 
        img === ""
            ? "https://cdn2.iconfinder.com/data/icons/e-commerce-line-4-1/1024/open_box4-512.png"
            : img;
    this.data = new Date();
}

const productArray = localStorage.getItem('localArray') ? JSON.parse(localStorage.getItem('localArray')) : [];

function create() {
    const name = document.getElementById('name').value;
    const category = document.getElementById('category').selectedOptions[0].value;
    const price = parseInt(document.getElementById('price').value);
    const valute = document.getElementById('valute').selectedOptions[0].value;
    const newOrUsed = document.querySelector("input[name=new_used]:checked").value;
    const img = document.getElementById('img').value;
    // product object
    const product = new Product(name, category, price, valute, newOrUsed, img);
    // product object array
    productArray.push(product);
    
    localStorage.setItem('localArray', JSON.stringify(productArray));
}
// window.onbeforeunload = function() {
//     return true;
// };