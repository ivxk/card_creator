
const checkUserLocal = localStorage.getItem('in');
const checkUserSession = sessionStorage.getItem('in');
if(!checkUserLocal && !checkUserSession)
    window.location.href = 'logIn.html';

const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : JSON.parse(sessionStorage.getItem('user'));

document.getElementById('helloSmn').innerHTML = `Hello, ${user.firstname}`;    
    
let filteredArray = [];
const cartArray = localStorage.getItem("cart")
    ? JSON.parse(localStorage.getItem("cart"))
    : [];
const productArray = localStorage.getItem('localArray') 
    ? JSON.parse(localStorage.getItem('localArray')) 
    : [];

let cheap = 0, exp = 0;
let newTime = 0, oldTime = 0;
let search = 0;
function sortPrice(){
    const price = document.getElementById('price').value;
    if(price === 'cheap')
        cheap = 1, exp = 0;
    else cheap = 0, exp = 1;
    // else cheap = 0, exp = 0;
    filter();
}
function sortTime(){
    const time = document.getElementById('time').value;
    if(time === 'new_to_old')
        newTime = 1, oldTime = 0;
    else oldTime = 1, newTime = 0;
    filter();
}
function searchByName(){
    search = 1;
    filter();
}
function resetSearch(){
    search = 0;
    filter();
}

window.addEventListener('load', displayCard(productArray));

function displayCard(array) {
    // showing count of choosen products
    const badge = document.querySelector('.badge');
    let counter = cartArray.length;
    // console.log(counter);
    badge.innerHTML = counter;

    const x = document.querySelector(".main");
    x.innerHTML = '';
    if(cheap)
    {
        array.sort(function(a, b)
            { return a.price - b.price});
    }
    else{
        array.sort(function(a, b)
            { return b.price - a.price});
    }
    if(newTime)
    {
        array.sort(function(a, b)
            {
                a.data = new Date(a.data); 
                // console.log(a);
                b.data = new Date(b.data); 
                // console.log(b);
                return b.data.getTime() - a.data.getTime();
            });
    }
    else{
        array.sort(function(a, b)
            { return a.data.getTime - b.data.getTime});
    }
    // console.log(array);
    array.forEach((product, index) => {
        x.innerHTML +=
            `<div class="card" style="width: 15rem;">
            <img src=${product.img} class="card-img-top" alt="404">
            <div class="card-body">
                <h5 class="card-title">${product.name}</h5>
                <p class="card-text">Category: <span style="font-weight: bolder">${product.category}</p>
                <p class="card-text">Price: <span style="font-weight: bolder">${product.price} ${product.valute}</p>
                <p class="card-text">Condition: <span style="font-weight: bolder">${product.newOrUsed}</p>
                <p class="card-text">Time: <span style="font-weight: bolder">${product.data.toLocaleString()}</p>
                <button class="btn btn-primary" onclick='countBuy(${index})'>Buy</button>
            </div>
        </div>`;
    });
}

function countBuy(index) {
    const product = productArray[index];
    cartArray.push(product);
    localStorage.setItem("cart", JSON.stringify(cartArray));
    
    const badge = document.querySelector('.badge');
    let counter = cartArray.length;
    // console.log(counter);
    badge.innerHTML = counter;
}

function filter() {
    const filt = document.getElementById('filter').selectedOptions[0].value;
    const filt1 = document.getElementById('filter1').selectedOptions[0].value;
    if (filt === 'all' && filt1 === 'none')
        filteredArray = productArray;
    else if(filt === 'all')
    {
        filteredArray = productArray.filter(product => product.newOrUsed === filt1);
    }
    else if(filt1 === 'none')
    {
        filteredArray = productArray.filter(product => product.category === filt);
    }
    else{
        filteredArray = productArray.filter(product => product.category === filt && product.newOrUsed === filt1);
    }
    if(search)
    {
        const searchName = document.getElementById('searchName').value;
        const namee = new RegExp(searchName, 'gi');
        // console.log(searchName);
        filteredArray = filteredArray.filter(product => {
            return product.name.match(namee);
        });
    }
    displayCard(filteredArray);
}

