document.querySelector('button').addEventListener('click', ()=>{
    const name = document.querySelector('#username').value;
    const password = document.querySelector('#password').value;
    // const users = JSON.parse(localStorage.getItem('users'));
    const users = localStorage.getItem("users")
    ? JSON.parse(localStorage.getItem("users"))
    : [{username: 'admin', password: 'Admin123'}];
    
    const bool = users.find(user => {
        return user.username === name && user.password === password;
    });
    
    if(bool)
    {
        const check = document.querySelector('.form-check-input').checked;
        if(check)
            localStorage.setItem('in', check), localStorage.setItem('user', JSON.stringify(bool));
        else
            sessionStorage.setItem('in', true), sessionStorage.setItem('user', JSON.stringify(bool));
            
        if(bool.username === 'admin' && bool.password === 'Admin123')
            window.location.href = 'admin.html';
        else window.location.href = 'user.html';
    }
    else{
        document.querySelector('.modal').style = 'display: block';
    }
});

document.querySelector('.btn-close').addEventListener('click', ()=>{
    document.querySelector('.modal').style = 'display: none';
});
document.querySelector('#close_btn').addEventListener('click', ()=>{
    document.querySelector('.modal').style = 'display: none';
});