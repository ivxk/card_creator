
const cartArray = JSON.parse(localStorage.getItem("cart"));

window.addEventListener('load', displayCard(cartArray));
// console.log(cartArray);

function displayCard(array) {
    // showing count of choosen products
    const badge = document.querySelector('.badge');
    let counter = cartArray.length;
    // console.log(counter);
    badge.innerHTML = counter;
    
    const x = document.querySelector('.main');
    x.innerHTML = '';
    array.forEach((product, index) => {
        x.innerHTML +=
            `<div class="card" style="width: 15rem;">
            <img src=${product.img} class="card-img-top" alt="404">
            <div class="card-body">
                <h5 class="card-title">${product.name}</h5>
                <p class="card-text">Category: <span style="font-weight: bolder">${product.category}</p>
                <p class="card-text">Price: <span style="font-weight: bolder">${product.price} ${product.valute}</p>
                <p class="card-text">Condition: <span style="font-weight: bolder">${product.newOrUsed}</p>
                <p class="card-text">Time: <span style="font-weight: bolder">${product.data.toLocaleString()}</p>
                <button class="btn btn-primary" onclick='removeProduct(${index})'>Remove</button>
            </div>
        </div>`;
    });
}

function removeProduct(index) {
    cartArray.splice(index, 1);
    displayCard(cartArray);
    localStorage.removeItem('cart', JSON.stringify(cartArray));
}  